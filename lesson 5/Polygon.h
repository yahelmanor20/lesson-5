#pragma once
#include "Shape.h"
#include "Point.h"
#include <iostream>
#include <vector>
//#include "Cimg.h"

using namespace std;
namespace myShapes {
	class Polygon : public Shape
	{
	public:
		Polygon();
		Polygon(const string& name, const string& type);
		virtual ~Polygon();
		virtual double getArea() const;
		virtual double getPerimeter() const;
		virtual void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
		virtual void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
		virtual void move(const Point& other);//rectancle an dtriangle dosent need move for them own the has in polygon

	protected:
		vector<Point> _points;
	};
}