#include "Menu.h"
using namespace std;
Menu::Menu()
{
	_board = new cimg_library::CImg<unsigned char>(700, 700, 1, 3, 1);
	_disp = new cimg_library::CImgDisplay(*_board, "Super Paint");
}
Menu::~Menu()
{
	//_disp->close();
	delete _board;
	delete _disp;
	//delete _myShapes;
}

int Menu::printMenu()
{
		int choise = 0, x = 0, y = 0,place = 0;
		int subChoise = 0;
		double raduis = 0;
		string name;
		size_t i;
	while (choise != 3)
	{
		cout << "Enter 0 to add a new shape." << endl;
		cout << "Enter 1 to modify or get information from a current shape." << endl;
		cout << "Enter 2 to delete all of the shapes." << endl;
		cout << "Enter 3 to exit." << endl;
		cin >> choise;
		//main switch
		switch (choise)
		{
		case 0:
		{
			cout << "Enter 0 to add a circle." << endl;
			cout << "Enter 1 to add an arrow." << endl;
			cout << "Enter 2 to add a triangle." << endl;
			cout << "Enter 3 to add a rectangle." << endl;
			cin >> subChoise;
		}
		//sub switch
		//there is {} to let the program allocate vars
			switch (subChoise)
			{
			case circle:
			{	
			cout << "Enter x: " << endl;
			cin >> x;
			cout << "Enter y: " << endl;
			cin >> y;
			cout << "Enter radius: " << endl;
			cin >> raduis;
			getchar();
			cout << "Enter the circle name: " << endl;
			getline(cin, name);

			Point a(x, y);
			Circle *  tmp = new Circle(a, raduis, name, "Circle");

			tmp->draw(*this->_disp,*this->_board);
			_Shapes.push_back(tmp);
			break;
			}
			case arrow:
			{
				cout << "Enter x1: " << endl;
				cin >> x;
				cout << "Enter y1: " << endl;
				cin >> y;
				Point a(x, y);
				cout << "Enter x2: " << endl;
				cin >> x;
				cout << "Enter y2: " << endl;
				cin >> y;
				getchar();
				Point b(x, y);
				cout << "Enter the arrow name: " << endl;
				getline(cin, name);

				Arrow * tmp = new Arrow(a, b, name, "Arrow");

				tmp->draw(*this->_disp, *this->_board);
				_Shapes.push_back(tmp);
				break;
			}
			case tringle:
			{
				cout << "Enter x1: " << endl;
				cin >> x;
				cout << "Enter y1: " << endl;
				cin >> y;
				Point a(x, y);
				cout << "Enter x2: " << endl;
				cin >> x;
				cout << "Enter y2: " << endl;
				cin >> y;
				Point b(x, y);
				cout << "Enter x3: " << endl;
				cin >> x;
				cout << "Enter y3: " << endl;
				cin >> y;
				Point c(x, y);
				getchar();
				
				if (a.getX() == b.getX() && a.getX() == c.getX() || a.getY() == b.getY() && a.getY()== c.getY())
				{
					cerr << "ERORR: cant allocate triagle";
				}
				cout << "Enter the triangle name: " << endl;
				getline(cin, name);

				Triangle *tmp = new Triangle(a, b, c, name, "Triangle");
				tmp->draw(*this->_disp, *this->_board);
				_Shapes.push_back(tmp);
				break;
			}
			case rectangle:
			{
				cout << "Enter x: " << endl;
				cin >> x;
				cout << "Enter y: " << endl;
				cin >> y;
				Point a(x, y);
				cout << "Enter lengh: " << endl;
				cin >> x;
				if (x<=0)
				{
					cerr << "ERORR: cant allocate revtangle";
				}
				cout << "Enter width: " << endl;
				cin >> y;
				if (y <= 0)
				{
					cerr << "ERORR: cant allocate revtangle";
				}
				getchar();
				cout << "Enter the rectangle name: " << endl;
				getline(cin, name);

				myShapes::Rectangle * tmp = new myShapes::Rectangle(a, x, y, "Rectangle", name);

				tmp->draw(*this->_disp, *this->_board);
				_Shapes.push_back(tmp);
				break;
			}
			default:
				break;
			}
			break;
		case 1:
			for (i = 0; i < _Shapes.size(); i++)
			{
				cout << i << " - to " << _Shapes[i]->getName() << "(" << _Shapes[i]->getType() << ")" << endl;
			}
			cin >> place;
			cout << "Enter 0 to move the shape"<<endl;
			cout << "Enter 1 to get its details."<<endl;
			cout << "Enter 2 to remove the shape." << endl;
			cin >> choise;
			switch (choise)
			{
			case 0:
			{cout << "Enter x: " << endl;
			cin >> x;
			cout << "Enter y: " << endl;
			cin >> y;
			_Shapes[place]->clearDraw(*this->_disp, *this->_board);
			_Shapes[place]->move(Point(x, y));
			for (i = 0; i < _Shapes.size(); i++)
			{
				_Shapes[i]->draw(*this->_disp, *this->_board);
			}
			break; }
			case 1:
				_Shapes[place]->printDetails();
				break;
			case 2:
				_Shapes[place]->clearDraw(*this->_disp, *this->_board);
				_Shapes.erase(_Shapes.begin()+place);
			for (i = 0; i < _Shapes.size(); i++)
				{
					_Shapes[i]->draw(*this->_disp, *this->_board);
				}
			default:
				break;
			}
			break;
		case 2:
			for (i = 0; i < _Shapes.size(); i++)
			{
				_Shapes[i]->clearDraw(*this->_disp,*this->_board);
			}
			break;
		case 3:
			return 0;
		default:
			break;
		}
	}
	return 0;
}
