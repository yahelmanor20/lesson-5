#pragma once
#include "Polygon.h"
using namespace myShapes;
class Triangle : public Polygon
{
public:
	Triangle(const Point& a, const Point& b, const Point& c, const string& type, const string& name);
	virtual ~Triangle();
	void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
	virtual double getArea() const;
	virtual double getPerimeter() const;
	//virtual void move(const Point& other);
};