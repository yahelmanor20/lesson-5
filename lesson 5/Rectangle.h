#pragma once
#include "Polygon.h"

namespace myShapes
{
	// Calling it MyRectangle becuase Rectangle is taken by global namespace.
	class Rectangle : public Polygon
	{
	private:
		double _length;
		double _width;
		Point _a;
		std::string _type;
		std::string _name;
	public:
		// There's a need only for the top left corner 
		Rectangle(const Point& a, double length, double width, const string& type, const string& name);
		virtual ~Rectangle();
		void draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
		void clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board);
		// override functions if need (virtual + pure virtual)
		virtual double getArea() const;
		virtual double getPerimeter() const;
		//virtual void move(const Point& other);

	};
}