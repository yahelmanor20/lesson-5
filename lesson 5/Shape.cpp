#include "Shape.h"

Shape::Shape()
{
}
Shape::Shape(const string & name, const string & type)
{
	_name = name;
	_type = type;
}
Shape::~Shape()
{
}
void Shape::printDetails() const
{
	cout << "name: " << _name << endl << "type: " << _type << endl << "primeter: " << this->getPerimeter() << endl << "area: " << this->getArea() << endl;
}
string Shape::getType() const
{
	return this->_type;
}
string Shape::getName() const
{
	return this->_name;
}
