#include "Point.h"

Point::Point()
{

}
Point::Point(double x, double y)
{
	_x = x;
	_y = y;
}
Point::~Point()
{
}
Point::Point(const Point& other)
{
	this->_x = other._x;
	this->_y = other._y;
}

//distance qute
double Point::distance(const Point& other) const
{
	return POSITIVE(sqrt((_x - other._x) - (_y - other._y)));
}
Point Point::operator+(const Point & other) const
{
	Point sum(other._x + _x, other._y + _y);
	return sum;
}
Point & Point::operator+=(const Point & other)
{
	_x += other._x;
	_y += other._y;
	return *this;
}
double Point::getX() const
{
	return this->_x;
}
double Point::getY() const
{
	return this->_y;
}