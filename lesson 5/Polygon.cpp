#include "Polygon.h"
using namespace myShapes;
myShapes::Polygon::Polygon()
{
	_points.resize(3);
}
Polygon::Polygon(const string & type, const string & name):Shape(type,name)
{
}
Polygon::~Polygon()
{
}
double Polygon::getArea() const
{
	return (POSITIVE(_points[0].getX() - _points[1].getX()) * _points[2].getX())/2;
}
double Polygon::getPerimeter() const
{
	return POSITIVE(_points[0].getX() - _points[1].getX() + POSITIVE(_points[1].getX() - _points[2].getX()) + POSITIVE(_points[0].getX() - _points[2].getX()));
}
void Polygon::draw(cimg_library::CImgDisplay & disp, cimg_library::CImg<unsigned char>& board)
{
	cout << "never";
}
void Polygon::clearDraw(cimg_library::CImgDisplay & disp, cimg_library::CImg<unsigned char>& board)
{
	cout << "never";
}
void Polygon::move(const Point& other)
{
	_points[0] += other;
}
