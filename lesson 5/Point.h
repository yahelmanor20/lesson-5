#pragma once
#include <vector>
#include <math.h>
//#include "CImg.h"
#define POSITIVE(n) ((n) < 0 ? 0 - (n) : (n))// makes every number to positive
class Point
{
private:
	double _x;
	double _y;
public:
	Point();
	Point(double x, double y);
	Point(const Point& other);
	virtual ~Point();

	Point operator+(const Point& other) const;
	Point& operator+=(const Point& other);

	double getX() const;
	double getY() const;	

	double distance(const Point& other) const;


};