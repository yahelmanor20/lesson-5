#pragma once
#include "Circle.h"
#include "Arrow.h"
#include "Triangle.h"
#include "Rectangle.h"
#include "CImg.h"
#include <vector>
#include <iostream>
#include <sstream>
#include "Polygon.h"
//enum for all situations
enum shapes{circle,arrow,tringle,rectangle};
class Menu
{
public:

	Menu();
	~Menu();

	int printMenu();

private:
	//strge of all the current Shapes that has been putted
	vector <Shape*> _Shapes;
	cimg_library::CImg<unsigned char>* _board;
	cimg_library::CImgDisplay* _disp;
};

