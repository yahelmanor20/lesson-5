#include "Circle.h"
//constractors
Circle::Circle(const Point & center, double radius, const string & type, const string & name):Shape(type, name)
{
	_center = center;
	_radius = radius;
}
Circle::~Circle()
{
}

//class functions
const Point & Circle::getCenter() const
{
	return _center;
}
double Circle::getRadius() const
{
	return _radius;
}

//pure functions
double Circle::getArea() const
{
	return (_radius * _radius) * PI;
}
double Circle::getPerimeter() const
{
	return (_radius * PI) * 2;
}
void Circle::move(const Point& other)
{
	this->_center += other;
}

//draw functions
void Circle::draw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLUE[] = { 0, 0, 255 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLUE, 100.0f).display(disp);	
}
void Circle::clearDraw(cimg_library::CImgDisplay& disp, cimg_library::CImg<unsigned char>& board)
{
	unsigned char BLACK[] = { 0, 0, 0 };
	const Point& c = getCenter();
	board.draw_circle(c.getX(), c.getY(), getRadius(), BLACK, 100.0f).display(disp);
}


